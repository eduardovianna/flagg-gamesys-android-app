package com.gamesys.flagg.bases

import com.gamesys.flagg.application.App
import kotlin.jvm.javaClass

/**
 * Created by eduardovianna on 22/06/18.
 */
open class BasePresenterImpl<V : BaseView> : BasePresenter<V> {

    protected val tag by lazy { javaClass.simpleName }

    protected val TAG: String
        get() {
            return tag
        }

    protected val context = App.getGlobalContext()
    protected var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }

    fun translate(resource: Any): String {
        if (resource is String) {
            return resource
        }
        else if (resource is Int) {
            return context?.getString(resource) ?: ""
        }
        return ""
    }

}