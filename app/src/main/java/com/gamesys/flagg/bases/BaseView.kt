package com.gamesys.flagg.bases

import android.content.Context
/**
 * Created by eduardovianna on 22/06/18.
 */
interface BaseView {

    fun getContext(): Context?

    fun showError(error: String?)

    fun showMessage(message: String)
}