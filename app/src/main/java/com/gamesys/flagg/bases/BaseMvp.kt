package com.gamesys.flagg.bases

import android.content.Context
import android.os.Bundle
import android.widget.Toast

/**
 * Created by eduardovianna on 24/06/18.
*/
abstract class BaseMvp<in V : BaseView, T : BasePresenter<V>> : BaseActivity(), BaseView {

    protected abstract var presenter: T

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this as V)
    }

    // MVP defaults methods

    override fun getContext(): Context = this

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}