package com.gamesys.flagg.bases

import com.gamesys.flagg.BuildConfig
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by eduardovianna on 24/06/18.
 */
abstract class BaseRetrofit<T>(val klass: Class<T>) {

    protected open fun baseUri(): String {
        return BuildConfig.COUNTRY_API
    }

    open fun token(): String? {
        return null
    }

    private var _api: T? = null

    protected val api: T
        get() {
            if (_api == null) {
                _api = retrofit.create(klass)
            }
            return _api!!
        }

    private val retrofit by lazy {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY


        val client = OkHttpClient
                        .Builder()
                        .readTimeout(30, TimeUnit.SECONDS)
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .addInterceptor { chain ->
                            isLoading = true
                            val response = chain.proceed(chain.request())
                            isLoading = false
                            response
                        }

        token()?.let {
            client
                .addInterceptor { chain ->
                    val original = chain.request()
                    val request = original
                                    .newBuilder()
                                    .header("Authorization", """Bearer $it""")
                                    .method(original.method(), original.body())
                                    .build()

                    chain.proceed(request)
                }
        }

        val gsonDate = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
        Retrofit
                .Builder()
                .client(client.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gsonDate))
                .baseUrl(baseUri())
                .build()
    }

    private var emitter: ObservableEmitter<Boolean>? = null

    private var isLoading = false
        set(value) {
            AndroidSchedulers.mainThread().run {
                emitter?.onNext(value)
            }
        }

    val loading: Observable<Boolean>
        get() {
            return Observable.create { emitter ->
                this.emitter = emitter
            }
        }
}