package com.gamesys.flagg.bases

/**
 * Created by eduardovianna on 22/06/18.
 */
interface BasePresenter<in V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}