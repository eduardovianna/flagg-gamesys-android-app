package com.gamesys.flagg.bases

import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.gamesys.flagg.R
import com.gamesys.flagg.extensions.translate
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.reflect.KClass

/**
 * Created by eduardovianna on 24/06/18.
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }


    fun alertDialogBuilder(title: Any? = null, message: Any? = null, showIcon: Boolean = true): AlertDialog.Builder {
        val builder = AlertDialog.Builder(this)
        if (showIcon)
            builder.setIcon(R.mipmap.ic_launcher)

        if (title != null)
            builder.setTitle(translate(title))

        if (message != null)
            builder.setMessage(translate(message))

        return builder
    }

    fun runActivity(to: KClass<*>, isFinishCurrentActivity: Boolean = false, intentParams: ((Intent) -> Unit)? = null) {
        val intent = Intent(this, to.java)

        intentParams?.invoke(intent)

        startActivity(intent)

        if (isFinishCurrentActivity) {
            finish()
        }
    }

    fun runOnMainThread(after: Long = 0, code: () -> Unit) {
        if (after.toInt() == 0) {
            runOnUiThread {
                code()
            }
        } else {
            Timer().schedule(timerTask {
                runOnUiThread {
                    code()
                }
            }, after)
        }
    }


}