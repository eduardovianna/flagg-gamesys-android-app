package com.gamesys.flagg.services

import com.gamesys.flagg.bases.BaseRetrofit
import com.gamesys.flagg.domain.Country
import com.gamesys.flagg.extensions.addThreads
import io.reactivex.Observable

/**
 * Created by eduardovianna on 24/06/18.
 */
class FlagApiImpl : BaseRetrofit<FlagApi>(FlagApi::class.java) {

    fun getCountries(): Observable<List<Country>> {
        return api
                .getCountries()
                .addThreads()
    }

}