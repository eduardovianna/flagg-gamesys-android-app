package com.gamesys.flagg.services

import com.gamesys.flagg.domain.Country
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by eduardovianna on 22/06/18.
 */
interface FlagApi {

    @GET("all")
    fun getCountries(): Observable<List<Country>>

}

