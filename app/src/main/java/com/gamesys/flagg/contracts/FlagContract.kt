package com.gamesys.flagg.contracts

import com.gamesys.flagg.bases.BasePresenter
import com.gamesys.flagg.bases.BaseView
import com.gamesys.flagg.domain.Country

/**
 * Created by eduardovianna on 23/06/18.
 */
class FlagContract {

    interface View : BaseView {

        fun hideError()
        fun displayError(error: String)
        fun initializeData(data: List<Country>)
        fun hideProgress()
        fun showProgress()
        fun openCountryDetail(country: Country)
    }

    interface Presenter : BasePresenter<View> {

        fun init()
        fun startLoading()
        fun onReloadPressed()
        fun onCountryClicked(country: Country)
    }
}