package com.gamesys.flagg.contracts

import com.gamesys.flagg.bases.BasePresenter
import com.gamesys.flagg.bases.BaseView
import com.gamesys.flagg.domain.Country
import com.gamesys.flagg.domain.Currency
import com.gamesys.flagg.domain.Language

/**
 * Created by eduardovianna on 28/06/18.
 */
class CountryDetailContract {

    interface View : BaseView {

        fun displayCountryData(country: Country)
        fun displayFlagImage(flagUrl: String)
        fun addLanguage(language: Language)
        fun addCurrency(currency: Currency)
        fun configMap(latlng: List<Double>)

    }

    interface Presenter : BasePresenter<View> {

        fun init(country: Country)
    }

}