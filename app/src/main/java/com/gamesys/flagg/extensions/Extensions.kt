package com.gamesys.flagg.extensions

import android.content.Context
import android.content.Intent
import android.graphics.drawable.PictureDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.gamesys.flagg.R
import com.gamesys.flagg.activities.CountryDetailActivity
import com.gamesys.flagg.application.App
import com.gamesys.flagg.domain.Country
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import com.bumptech.glide.Glide
import com.gamesys.flagg.glide.GlideApp
import com.gamesys.flagg.glide.SvgSoftwareLayerSetter


/**
 * Created by eduardovianna on 24/06/18.
 */

/**
 * Translate
 */

val Int.translate: String
    get() {
        return App.context?.getString(this) ?: ""
    }

fun translate(message: Any): String {
    if (message is String) {
        return message
    } else if (message is Int) {
        return App.context?.getString(message) ?: ""
    }
    return ""
}

/**
 * Observable
 */

fun <T> Observable<T>.addThreads(): Observable<T> {
    return this
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

/**
 * Handle Error
 */

data class ResponseParser(val success: Boolean, val error: String, val code: Int = 404)

val Throwable.parser: ResponseParser
    get() {
        if (this !is HttpException) {
            return ResponseParser(false, R.string.error_message.translate)
        }

        val strError = this.response().errorBody()?.string() ?: return ResponseParser(false, R.string.error_message.translate)

        val gson = Gson()
        val typeAdapter = gson.getAdapter(ResponseParser::class.java)
        return try {
            typeAdapter.fromJson(strError)
        } catch (e: IOException) {
            ResponseParser(false, R.string.error_message.translate)
        }
    }

fun RecyclerView.setupList(contex: Context?, adapter: RecyclerView.Adapter<*>) {
    val layoutManager = LinearLayoutManager(context)
    this.layoutManager = layoutManager
    this.adapter = adapter

}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun getRequestBuilder(context: Context): RequestBuilder<PictureDrawable> {

    return GlideApp.with(context)
        .`as`(PictureDrawable::class.java)
        .listener(SvgSoftwareLayerSetter())
}

fun Context.countryDetailIntent(country: Country): Intent {
    return Intent(this, CountryDetailActivity::class.java).apply {
        putExtra(CountryDetailActivity.COUNTRY_DATA, country)
    }
}