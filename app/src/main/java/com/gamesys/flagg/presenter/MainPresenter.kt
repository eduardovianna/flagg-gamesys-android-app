package com.gamesys.flagg.presenter

import com.gamesys.flagg.bases.BasePresenterImpl
import com.gamesys.flagg.contracts.FlagContract
import com.gamesys.flagg.domain.Country
import com.gamesys.flagg.extensions.parser
import com.gamesys.flagg.services.FlagApiImpl

/**
 * Created by eduardovianna on 22/06/18.
 */
class MainPresenter : BasePresenterImpl<FlagContract.View>(), FlagContract.Presenter {

    private var serviceApi = FlagApiImpl()

    override fun init() {
        getCountries()
    }

    private fun getCountries() {
        startLoading()
        serviceApi
                .getCountries()
                .subscribe({
                    onSuccessLoad(it)
                }, {
                    onErrorLoad(it.parser.error)
                })
    }

    override fun startLoading() {
        view?.showProgress()
    }

    private fun onErrorLoad(error: String) {
        view?.displayError(error)
    }


    private fun onSuccessLoad(countries: List<Country>) {
        view?.hideProgress()
        view?.initializeData(countries)

    }

    override fun onReloadPressed() {
        getCountries()
    }

    override fun onCountryClicked(country: Country) {
        view?.openCountryDetail(country)
    }

}