package com.gamesys.flagg.presenter

import com.gamesys.flagg.bases.BasePresenterImpl
import com.gamesys.flagg.contracts.CountryDetailContract
import com.gamesys.flagg.domain.Country

/**
 * Created by eduardovianna on 28/06/18.
 */
class CountryDetailPresenter : BasePresenterImpl<CountryDetailContract.View>(), CountryDetailContract.Presenter {


    override fun init(country: Country) {

        view?.displayFlagImage(country.flag)
        view?.displayCountryData(country)

        country.languages?.forEach {
            view?.addLanguage(it)
        }

        country.currencies?.forEach {
            view?.addCurrency(it)
        }

        country.latlng?.let {
            view?.configMap(it)
        }
    }


}