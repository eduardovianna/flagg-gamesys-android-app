package com.gamesys.flagg.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by eduardovianna on 22/06/18.
 */
class RegionalBloc() : Parcelable {

    var acronym: String? = null
    var name: String? = null
    var otherAcronyms: List<Any>? = null
    var otherNames: List<Any>? = null
    private val additionalProperties = HashMap<String, Any>()

    constructor(parcel: Parcel) : this() {
        acronym = parcel.readString()
        name = parcel.readString()
    }

    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(acronym)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RegionalBloc> {
        override fun createFromParcel(parcel: Parcel): RegionalBloc {
            return RegionalBloc(parcel)
        }

        override fun newArray(size: Int): Array<RegionalBloc?> {
            return arrayOfNulls(size)
        }
    }

}