package com.gamesys.flagg.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by eduardovianna on 22/06/18.
 */
class Language() : Parcelable {

    var iso6391: String? = null
    var iso6392: String? = null
    var name: String? = null
    var nativeName: String? = null
    private val additionalProperties = HashMap<String, Any>()

    constructor(parcel: Parcel) : this() {
        iso6391 = parcel.readString()
        iso6392 = parcel.readString()
        name = parcel.readString()
        nativeName = parcel.readString()
    }

    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(iso6391)
        parcel.writeString(iso6392)
        parcel.writeString(name)
        parcel.writeString(nativeName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Language> {
        override fun createFromParcel(parcel: Parcel): Language {
            return Language(parcel)
        }

        override fun newArray(size: Int): Array<Language?> {
            return arrayOfNulls(size)
        }
    }

}