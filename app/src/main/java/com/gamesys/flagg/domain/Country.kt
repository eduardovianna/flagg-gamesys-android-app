package com.gamesys.flagg.domain

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.HashMap

/**
 * Created by eduardovianna on 22/06/18.
 */

class Country() : Parcelable {

    var name: String = ""
    var topLevelDomain: List<String>? = null
    var alpha2Code: String? = null
    var alpha3Code: String? = null
    var callingCodes: List<String>? = null
    var capital: String? = null
    var altSpellings: List<String>? = null
    var region: String? = null
    var subregion: String? = null
    var population: Int? = null
    var latlng: List<Double> = arrayListOf()
    var demonym: String? = null
    var area: Double? = null
    var gini: Double? = null
    var timezones: List<String>? = null
    var borders: List<String>? = null
    var nativeName: String? = null
    var numericCode: String? = null
    var currencies: List<Currency>? = null
    var languages: List<Language>? = null
    var translations: Translations? = null
    var flag: String = ""
    var regionalBlocs: List<RegionalBloc>? = null
    var cioc: String? = null
    private val additionalProperties = HashMap<String, Any>()

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        topLevelDomain = parcel.createStringArrayList()
        alpha2Code = parcel.readString()
        alpha3Code = parcel.readString()
        callingCodes = parcel.createStringArrayList()
        capital = parcel.readString()
        altSpellings = parcel.createStringArrayList()
        region = parcel.readString()
        subregion = parcel.readString()
        population = parcel.readValue(Int::class.java.classLoader) as? Int
        latlng = parcel.createDoubleArray().toList()
        demonym = parcel.readString()
        area = parcel.readValue(Double::class.java.classLoader) as? Double
        gini = parcel.readValue(Double::class.java.classLoader) as? Double
        timezones = parcel.createStringArrayList()
        borders = parcel.createStringArrayList()
        nativeName = parcel.readString()
        numericCode = parcel.readString()
        currencies = parcel.createTypedArrayList(Currency)
        languages = parcel.createTypedArrayList(Language)
        flag = parcel.readString()
        cioc = parcel.readString()
    }

    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeStringList(topLevelDomain)
        parcel.writeString(alpha2Code)
        parcel.writeString(alpha3Code)
        parcel.writeStringList(callingCodes)
        parcel.writeString(capital)
        parcel.writeStringList(altSpellings)
        parcel.writeString(region)
        parcel.writeString(subregion)
        parcel.writeValue(population)
        val latlngArray = latlng.toDoubleArray()
        parcel.writeDoubleArray(latlngArray)
        parcel.writeString(demonym)
        parcel.writeValue(area)
        parcel.writeValue(gini)
        parcel.writeStringList(timezones)
        parcel.writeStringList(borders)
        parcel.writeString(nativeName)
        parcel.writeString(numericCode)
        parcel.writeTypedList<Currency>(currencies)
        parcel.writeTypedList<Language>(languages)
        parcel.writeString(flag)
        parcel.writeString(cioc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Country> {
        override fun createFromParcel(parcel: Parcel): Country {
            return Country(parcel)
        }

        override fun newArray(size: Int): Array<Country?> {
            return arrayOfNulls(size)
        }
    }

}