package com.gamesys.flagg.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by eduardovianna on 22/06/18.
 */
class Translations() : Parcelable {

    var de: String? = null
    var es: String? = null
    var fr: String? = null
    var ja: String? = null
    var it: String? = null
    var br: String? = null
    var pt: String? = null
    var nl: String? = null
    var hr: String? = null
    var fa: String? = null
    private val additionalProperties = HashMap<String, Any>()

    constructor(parcel: Parcel) : this() {
        de = parcel.readString()
        es = parcel.readString()
        fr = parcel.readString()
        ja = parcel.readString()
        it = parcel.readString()
        br = parcel.readString()
        pt = parcel.readString()
        nl = parcel.readString()
        hr = parcel.readString()
        fa = parcel.readString()
    }

    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(de)
        parcel.writeString(es)
        parcel.writeString(fr)
        parcel.writeString(ja)
        parcel.writeString(it)
        parcel.writeString(br)
        parcel.writeString(pt)
        parcel.writeString(nl)
        parcel.writeString(hr)
        parcel.writeString(fa)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Translations> {
        override fun createFromParcel(parcel: Parcel): Translations {
            return Translations(parcel)
        }

        override fun newArray(size: Int): Array<Translations?> {
            return arrayOfNulls(size)
        }
    }
}