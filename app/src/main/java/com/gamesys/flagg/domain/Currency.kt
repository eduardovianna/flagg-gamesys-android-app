package com.gamesys.flagg.domain

import android.os.Parcel
import android.os.Parcelable
import java.util.HashMap

/**
 * Created by eduardovianna on 22/06/18.
 */

class Currency() : Parcelable {

    var code: String? = null
    var name: String? = null
    var symbol: String? = null
    private val additionalProperties = HashMap<String, Any>()

    constructor(parcel: Parcel) : this() {
        code = parcel.readString()
        name = parcel.readString()
        symbol = parcel.readString()
    }

    fun getAdditionalProperties(): Map<String, Any> {
        return this.additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        this.additionalProperties.put(name, value)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(code)
        parcel.writeString(name)
        parcel.writeString(symbol)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Currency> {
        override fun createFromParcel(parcel: Parcel): Currency {
            return Currency(parcel)
        }

        override fun newArray(size: Int): Array<Currency?> {
            return arrayOfNulls(size)
        }
    }

}
