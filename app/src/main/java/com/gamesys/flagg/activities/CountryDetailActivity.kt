package com.gamesys.flagg.activities

import android.graphics.drawable.PictureDrawable
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.widget.TextView
import com.bumptech.glide.RequestBuilder
import com.gamesys.flagg.R
import com.gamesys.flagg.bases.BaseMvp
import com.gamesys.flagg.contracts.CountryDetailContract
import com.gamesys.flagg.domain.Country
import com.gamesys.flagg.domain.Currency
import com.gamesys.flagg.domain.Language
import com.gamesys.flagg.extensions.getRequestBuilder
import com.gamesys.flagg.glide.GlideApp
import com.gamesys.flagg.presenter.CountryDetailPresenter
import kotlinx.android.synthetic.main.content_scroll.*
import kotlinx.android.synthetic.main.country_detail.*


/**
 * Created by eduardovianna on 27/06/18.
 */
class CountryDetailActivity : BaseMvp<CountryDetailContract.View, CountryDetailContract.Presenter>(), CountryDetailContract.View {

    override var presenter: CountryDetailContract.Presenter = CountryDetailPresenter()
    private var requestBuilder: RequestBuilder<PictureDrawable>? = null


    companion object {
        val COUNTRY_DATA = "COUNTRY_DATA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initActivityTransitions()
        setContentView(R.layout.country_detail)

        val bundle = intent.extras
        val country = bundle.getParcelable<Country>(COUNTRY_DATA)
        requestBuilder = getRequestBuilder(this)


        supportPostponeEnterTransition()

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        collapsing_toolbar.title = country.name
        collapsing_toolbar.setExpandedTitleColor(resources.getColor(android.R.color.transparent))

        presenter.init(country)
    }

    private fun initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val transition = Slide()
            transition.excludeTarget(android.R.id.statusBarBackground, true)
            window.enterTransition = transition
            window.returnTransition = transition
        }
    }

    override fun displayCountryData(country: Country) {
        region_name.text = country.region
        capital_name.text = country.capital
        population_value.text = "${country.population}"

    }

    override fun displayFlagImage(flagUrl: String) {
        requestBuilder?.let {
            it.load(flagUrl)
                    .into(flag_image)
        }
    }

    override fun addLanguage(language: Language) {
        val tvLanguage = TextView(this)
        tvLanguage.text = language.name

        language_list.addView(tvLanguage)

    }

    override fun addCurrency(currency: Currency) {

        val tvCurrency = TextView(this)
        tvCurrency.text = currency.name

        currency_list.addView(tvCurrency)
    }

    override fun configMap(latlng: List<Double>) {

        val mapKey = resources.getString(R.string.MAPS_API_KEY)
        val mapUrl = resources.getString(R.string.MAP_API_URL, latlng[0], latlng[1], mapKey)

        GlideApp.with(this)
                .load(mapUrl)
                .centerCrop()
                .into(map_image)

    }

}