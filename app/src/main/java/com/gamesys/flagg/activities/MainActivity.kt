package com.gamesys.flagg.activities

import android.os.Bundle
import android.view.View
import com.gamesys.flagg.R
import com.gamesys.flagg.adapters.FlagAdapter
import com.gamesys.flagg.application.App.Companion.context
import com.gamesys.flagg.bases.BaseMvp
import com.gamesys.flagg.contracts.FlagContract
import com.gamesys.flagg.domain.Country
import com.gamesys.flagg.extensions.countryDetailIntent
import com.gamesys.flagg.extensions.hide
import com.gamesys.flagg.extensions.setupList
import com.gamesys.flagg.extensions.show
import com.gamesys.flagg.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by eduardovianna on 22/06/18.
 */
class MainActivity : BaseMvp<FlagContract.View, FlagContract.Presenter>(), FlagContract.View {

    override var presenter: FlagContract.Presenter = MainPresenter()

    private val adapter = FlagAdapter(arrayListOf()) { country: Country, v: View ->
        presenter.onCountryClicked(country)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter.init()
        setupRecyclerView()

        reload_button.setOnClickListener {
            presenter.onReloadPressed()
        }
    }

    private fun setupRecyclerView() {
        category_list.setupList(context, adapter)
    }

    override fun hideError() {
        error_view.hide()
    }

    override fun displayError(error: String) {
        service_loading.hide()
        error_view.show()
    }

    override fun initializeData(data: List<Country>) {
        adapter?.let {
            it.updateValues(data)
            it.notifyItemRangeChanged(0, it.itemCount)
            category_list.requestFocus()
            category_list.setHasFixedSize(true)
        }
    }

    override fun hideProgress() {
        service_loading.hide()
        category_list.show()
    }

    override fun showProgress() {
        hideError()
        category_list.hide()
        service_loading.show()
    }

    override fun openCountryDetail(country:Country){
        startActivity(this?.countryDetailIntent(country))
    }
}
