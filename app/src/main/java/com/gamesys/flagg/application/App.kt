package com.gamesys.flagg.application

import android.app.Application
import android.content.Context

/**
 * Created by eduardovianna on 22/06/18.
 */
class App : Application() {

    companion object {
        var context: Context? = null

        fun getGlobalContext(): Context? {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        App.context = applicationContext
    }

}