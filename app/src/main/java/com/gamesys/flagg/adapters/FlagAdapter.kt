package com.gamesys.flagg.adapters

import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.RequestBuilder
import com.gamesys.flagg.R
import com.gamesys.flagg.domain.Country
import com.gamesys.flagg.extensions.getRequestBuilder
import kotlinx.android.synthetic.main.item_flag.view.*


/**
 * Created by eduardovianna on 23/06/18.
 */
class FlagAdapter : RecyclerView.Adapter<FlagAdapter.ViewHolder> {

    private val items: ArrayList<Country>
    private val itemClick: (Country, View) -> Unit

    constructor(items: ArrayList<Country>, itemClick: (Country, View) -> Unit) : super() {
        this.itemClick = itemClick
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_flag, parent, false), this.itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View, private val itemClick: (Country, View) -> Unit) : RecyclerView.ViewHolder(itemView) {

        fun bind(country: Country) = with(itemView) {
            itemView.tv_flag_name.text = country.name
            itemView.setOnClickListener { itemClick(country, itemView) }

            val uri = Uri.parse(country.flag)

            val requestBuilder: RequestBuilder<PictureDrawable> = getRequestBuilder(context)

            requestBuilder
                    .load(uri)
                    .into(itemView.flag_image)
        }
    }

    fun updateValues(services: List<Country>) {
        items.clear()
        items.addAll(services)
        notifyDataSetChanged()
    }

}